" Special
let wallpaper  = "artistic-geometry-red-white-i2-5120x2880.jpg"
let background = "#0a0a0a"
let foreground = "#f2eded"
let cursor     = "#f2eded"

" Colors
let color0  = "#0a0a0a"
let color1  = "#e72c17"
let color2  = "#b87371"
let color3  = "#b58788"
let color4  = "#bdbaba"
let color5  = "#d0bab9"
let color6  = "#dedede"
let color7  = "#f2eded"
let color8  = "#aca8a8"
let color9  = "#e72c17"
let color10 = "#b87371"
let color11 = "#b58788"
let color12 = "#bdbaba"
let color13 = "#d0bab9"
let color14 = "#dedede"
let color15 = "#f2eded"
