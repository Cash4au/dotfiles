static const char norm_fg[] = "#f2eded";
static const char norm_bg[] = "#0a0a0a";
static const char norm_border[] = "#aca8a8";

static const char sel_fg[] = "#f2eded";
static const char sel_bg[] = "#b87371";
static const char sel_border[] = "#f2eded";

static const char urg_fg[] = "#f2eded";
static const char urg_bg[] = "#e72c17";
static const char urg_border[] = "#e72c17";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
