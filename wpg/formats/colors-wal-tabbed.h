static const char* selbgcolor   = "#0a0a0a";
static const char* selfgcolor   = "#f2eded";
static const char* normbgcolor  = "#b87371";
static const char* normfgcolor  = "#f2eded";
static const char* urgbgcolor   = "#e72c17";
static const char* urgfgcolor   = "#f2eded";
