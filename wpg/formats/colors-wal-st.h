const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#0a0a0a", /* black   */
  [1] = "#e72c17", /* red     */
  [2] = "#b87371", /* green   */
  [3] = "#b58788", /* yellow  */
  [4] = "#bdbaba", /* blue    */
  [5] = "#d0bab9", /* magenta */
  [6] = "#dedede", /* cyan    */
  [7] = "#f2eded", /* white   */

  /* 8 bright colors */
  [8]  = "#aca8a8",  /* black   */
  [9]  = "#e72c17",  /* red     */
  [10] = "#b87371", /* green   */
  [11] = "#b58788", /* yellow  */
  [12] = "#bdbaba", /* blue    */
  [13] = "#d0bab9", /* magenta */
  [14] = "#dedede", /* cyan    */
  [15] = "#f2eded", /* white   */

  /* special colors */
  [256] = "#0a0a0a", /* background */
  [257] = "#f2eded", /* foreground */
  [258] = "#f2eded",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
